use wasm_bindgen::prelude::*;
use y3network_model::adapters::Adapter;
use js_sys::Array;
use async_trait::async_trait;
use web_sys::console;

pub struct Cockpit;

#[async_trait(?Send)]
impl Adapter for Cockpit {
    async fn spawn(&self, cmd: &str, args: &[&str]) -> Result<String, anyhow::Error> {
        let parts = Array::new();
        parts.push(&JsValue::from(cmd));
        let args_array: Array = args.iter().copied().map(JsValue::from).collect();

        let promise = cockpit_js::spawn(
            parts.concat(&args_array), JsValue::from_str("{{ superuser: true }}")
        );

        let output = promise.await;

        let msg = format!("result: {:?}", output);
        console::log_1(&JsValue::from(msg));
        let value: String = output.unwrap().as_string().unwrap();
        Ok(value)
    }
}
