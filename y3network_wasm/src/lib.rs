mod adapters;

use wasm_bindgen::prelude::*;
use y3network_model::{Network,NetworkConfig,backends::Wicked};
use crate::adapters::Cockpit;
use js_sys::Array;

#[wasm_bindgen]
extern "C" {
    #[wasm_bindgen(js_namespace = console,js_name = log)]
    fn console_log(s: &str);

    #[wasm_bindgen(js_namespace = console,js_name = error)]
    fn console_error(s: &str);
}

/// Run a command and returns the output
#[wasm_bindgen]
pub async fn run_cmd(cmd: Array, superuser: bool) -> Result<JsValue,JsValue> {
    let options = format!("{{ superuser: {} }}", superuser);
    let options = JsValue::from_str(&options);
    cockpit_js::spawn(cmd, options).await
}


#[wasm_bindgen]
pub struct NetworkWrapper {
    network: Network
}

#[wasm_bindgen]
impl NetworkWrapper {
    #[wasm_bindgen(constructor)]
    pub fn new() -> Self {
        Default::default()
    }

    // pub async fn read(&mut self) {
    //     let backend = Wicked::new(Cockpit{});
    //     self.network.read(backend)
    // }

    pub fn get(&self, name: String) -> JsValue {
        let config = self.network.get(&name).unwrap();
        JsValue::from_serde(&config).unwrap()
    }

    pub fn add(&mut self, name: String, config: &JsValue) {
        let network_config: NetworkConfig = config.into_serde().unwrap();
        self.network.add(name, network_config);
    }
}

impl Default for NetworkWrapper {
    fn default() -> Self {
        Self {
            network: Network::new()
        }
    }
}

#[wasm_bindgen]
pub async fn fetch_wicked_config() -> JsValue {
    let mut network = Network::new();
    let backend = Wicked::new(Cockpit{});
    network.read(backend).await.unwrap();

    let config = network.get("system").unwrap();
    JsValue::from_serde(&config).unwrap()
}
