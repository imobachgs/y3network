const rust = import('y3network_wasm');

let api = {};
let wasm;

function utf8(str) {
    return window.unescape(encodeURIComponent(str));
}

api.login = function(username, password) {
    const headers = {
        Authorization: "Basic " + window.btoa(utf8(`${username}:${password}`)),
        "X-Superuser": "any",
        "X-Forwarded-Proto": "http",
        "Host": "localhost"
    };

    return fetch('http://localhost/cockpit/cockpit/login', {
        headers,
        credentials: 'include',
        method: 'GET'
    });
}

api.spawn = function(cmd, superuser) {
    return wasm.run_cmd(cmd.split(" "), superuser);
}

api.fetch_wicked_config = function() {
    return wasm.fetch_wicked_config();
}

api.loadWasm = function() {
    return new Promise((resolve) => {
        rust.then(m => {
            wasm = m;
            resolve(m);
        });
    });
}

window.api = api;
