use wasm_bindgen::prelude::*;
use js_sys::Array;

#[wasm_bindgen]
extern "C" {
    #[wasm_bindgen(js_namespace = cockpit)]
    pub fn format_number(number: f32) -> String;

    #[wasm_bindgen(js_namespace = cockpit,catch)]
    pub async fn spawn(command: Array, options: JsValue) -> Result<JsValue, JsValue>;
}

