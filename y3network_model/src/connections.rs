use crate::utils::Address;
use std::str::FromStr;
use serde::{Serialize,Deserialize};

/// It represents a connection definition.
#[derive(Default,Serialize,Deserialize)]
pub struct Connection {
    pub name: String,
    pub start_mode: StartMode,
    pub addresses: Vec<Address>,
    pub config: Config
}

impl Connection {
    pub fn new(name: String) -> Self {
        Self { name, ..Default::default() }
    }

    pub fn new_ethernet(name: String) -> Self {
        Self {
            name,
            config: Config::Ethernet,
            ..Default::default()
        }
    }

    pub fn new_wifi(name: String) -> Self {
        Self {
            name,
            config: Config::Wifi(WifiConfig::default()),
            ..Default::default()
        }
    }
}


/// Connection configuration
#[derive(Debug,Serialize,Deserialize)]
pub enum Config {
    Wifi(WifiConfig),
    Ethernet,
    Unknown
}

impl Default for Config {
    fn default() -> Self { Self::Ethernet }
}

/// Wifi configuration settings
#[derive(Debug,Default,Serialize,Deserialize)]
pub struct WifiConfig {
    pub essid: String,
    pub mode: WifiMode
}

/// Device start mode
#[derive(Serialize,Deserialize)]
pub enum StartMode {
    /// The connection is disabled
    Off,
    /// The connection should be started manually
    Manual,
    /// The connection starts on boot
    Auto
}

impl Default for StartMode {
    fn default() -> Self { StartMode::Manual }
}

/// Wifi mode
#[derive(Debug,Serialize,Deserialize)]
pub enum WifiMode {
    AdHoc,
    Ap,
    Infrastructure
}

impl Default for WifiMode {
    fn default() -> Self { WifiMode::Infrastructure }
}

impl FromStr for WifiMode {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "ad-hoc" => Ok(WifiMode::AdHoc),
            "ap" => Ok(WifiMode::Ap),
            "infrastructure" => Ok(WifiMode::Infrastructure),
            _ => Err(format!("Unknown wifi mode: {}", &s))
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_ethernet() {
        let eth0 = Connection::new_ethernet("eth0".to_string());
        assert_eq!(eth0.name, "eth0");
    }

    #[test]
    fn test_wifi() {
        let wlo1 = Connection::new_wifi("wlo1".to_string());
        assert_eq!(wlo1.name, "wlo1");
    }
}
