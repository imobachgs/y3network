use std::process::Command;
use async_trait::async_trait;

#[async_trait(?Send)]
pub trait Adapter {
    async fn spawn(&self, cmd: &str, args: &[&str]) -> Result<String, anyhow::Error>;
}

pub struct Local;

#[async_trait(?Send)]
impl Adapter for Local {
    // TODO: return a proper struct containing stdout, stderr and so on
    async fn spawn(&self, cmd: &str, args: &[&str]) -> Result<String, anyhow::Error> {
        let output = Command::new(cmd)
            .args(args)
            .output()?;
        let output = String::from_utf8(output.stdout)?;
        Ok(output)
    }
}
