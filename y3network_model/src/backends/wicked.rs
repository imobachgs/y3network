use crate::NetworkConfig;
use crate::connections::Connection;
use crate::devices::Device;
use serde_xml_rs::from_str;
use super::xml::{ObjectRoot,InterfaceRoot};
use crate::adapters::Adapter;
use super::Backend;
use async_trait::async_trait;

pub struct Wicked<T: Adapter> {
    adapter: T
}

#[async_trait(?Send)]
impl<T: Adapter> Backend<T> for Wicked<T> {
    /// Read network configuration from Wicked
    async fn read(&self) -> Result<NetworkConfig, anyhow::Error> {
        let devices = self.read_devices().await?;
        let conns = self.read_connections().await?;
        let config = NetworkConfig { devices, conns };
        Ok(config)
    }

    fn adapter(&self) -> &T {
        &self.adapter
    }
}

impl<T: Adapter> Wicked<T> {
    pub fn new(adapter: T) -> Self {
        Self {
            adapter
        }
    }

    async fn read_connections(&self) -> Result<Vec<Connection>, anyhow::Error> {
        let xml = self.wicked_cmd("show-config").await?;
        let parsed: Vec<InterfaceRoot> = from_str(&xml)?;
        let connections = parsed.into_iter().map(|i| i.into_connection()).collect();
        Ok(connections)
    }

    async fn read_devices(&self) -> Result<Vec<Device>, anyhow::Error> {
        let xml = self.wicked_cmd("show-xml").await?;
        let parsed: Vec<ObjectRoot> = from_str(&xml)?;
        let devices = parsed.into_iter().map(|i| i.into_device()).collect();
        Ok(devices)
    }

    async fn wicked_cmd(&self, cmd: &str) -> Result<String, anyhow::Error> {
        let args = [cmd];
        let output = self.adapter.spawn("/usr/sbin/wicked", &args).await?;
        let output = output
            .replace("ipv4:", "ipv4_")
            .replace("ipv6:", "ipv6_");
        Ok(output)
    }
}
