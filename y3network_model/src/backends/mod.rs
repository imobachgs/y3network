mod wicked;
pub use wicked::Wicked;
mod xml;

use crate::NetworkConfig;
use crate::adapters::Adapter;
use async_trait::async_trait;

#[async_trait(?Send)]
pub trait Backend<T: Adapter>
{
    /// Read network configuration from Wicked
    async fn read(&self) -> Result<NetworkConfig, anyhow::Error>;

    fn adapter(&self) -> &T;
}
