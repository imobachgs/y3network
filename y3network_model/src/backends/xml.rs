use serde::Deserialize;
use std::str::FromStr;
use crate::devices::{Device,DeviceType};
use crate::utils::Address;
use crate::connections::{Connection,Config,WifiConfig};

#[derive(Debug,Deserialize,PartialEq)]
pub struct ObjectRoot {
    pub path: String,
    pub interface: InterfaceNode,
    ipv4_static: Option<IpStatic>,
    ipv4_dhcp: Option<IpDhcp>
}

impl ObjectRoot {
    pub fn into_device(self) -> Device {
        let mut device = Device {
            name: self.interface.name,
            dev_type: DeviceType::from_str(&self.interface.link_type).unwrap(),
            ..Default::default()
        };

        if let Some(addresses) = self.interface.addresses {
            device.addresses = addresses.into_addresses();
        }
        device
    }
}

#[derive(Debug,Deserialize,PartialEq)]
pub struct InterfaceNode {
    pub name: String,
    pub index: usize,
    pub mtu: usize,
    #[serde(rename="link-type")]
    pub link_type: String,
    pub addresses: Option<AddressesNode>
}

#[derive(Debug,Deserialize,PartialEq)]
pub struct IpStatic {
    pub enabled: Option<bool>
}

#[derive(Debug,Deserialize,PartialEq)]
pub struct IpDhcp {
    pub enabled: Option<bool>
}

#[derive(Debug,Deserialize,PartialEq)]
pub struct InterfaceRoot {
    pub origin: String,
    pub name: String,
    pub ipv4_static: Option<AddressesNode>,
    pub ipv6_static: Option<AddressesNode>,
    pub wireless: Option<WirelessNode>
}

impl InterfaceRoot {
    pub fn into_connection(self) -> Connection {
        let config = if let Some(wireless) = self.wireless {
            Config::Wifi(wireless.into_wifi_config())
        } else {
            Config::Ethernet
        };

        let mut conn = Connection::new(self.name);
        conn.config = config;

        if let Some(ipv4_addresses) = self.ipv4_static {
            conn.addresses.append(&mut ipv4_addresses.into_addresses());
        }

        if let Some(ipv6_addresses) = self.ipv6_static {
            conn.addresses.append(&mut ipv6_addresses.into_addresses());
        }

        conn
    }
}

#[derive(Debug,Deserialize,PartialEq)]
pub struct AddressNode {
    pub local: String,
    pub label: Option<String>
}

impl AddressNode {
    pub fn into_address(self) -> Address {
        let mut address: Address = self.local.parse().unwrap();
        address.label = self.label;
        address
    }
}

#[derive(Debug,Deserialize,PartialEq)]
pub struct AddressesNode {
    #[serde(alias="assigned-address",alias="address")]
    pub addresses: Option<Vec<AddressNode>>
}

impl AddressesNode {
    pub fn into_addresses(self) -> Vec<Address> {
        match self.addresses {
            Some(addr) => addr.into_iter().map(|i| i.into_address()).collect(),
            None => Vec::new()
        }
    }
}

#[derive(Debug,Deserialize,PartialEq)]
pub struct WirelessNode {
    pub network: Option<WirelessNetworkNode>
}

impl WirelessNode {
    pub fn into_wifi_config(self) -> WifiConfig {
        if let Some(network) = self.network {
            network.into_wifi_config()
        } else {
            WifiConfig::default()

        }
    }
}

#[derive(Debug,Deserialize,PartialEq)]
pub struct WirelessNetworkNode {
    pub essid: String,
    pub mode: String,
}

impl WirelessNetworkNode {
    pub fn into_wifi_config(self) -> WifiConfig {
        WifiConfig {
            essid: self.essid,
            mode: self.mode.parse().unwrap_or_default()
        }
    }
}
