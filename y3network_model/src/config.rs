use super::connections::Connection;
use super::devices::Device;
use serde::{Serialize,Deserialize};
use crate::backends::{Backend,Wicked};
use crate::adapters::Local;

/// Represents the network configuration, including connections and network devices.
#[derive(Default,Serialize,Deserialize)]
pub struct NetworkConfig {
    pub conns: Vec<Connection>,
    pub devices: Vec<Device>
}

impl NetworkConfig {
    pub fn new() -> NetworkConfig {
        Default::default()
    }

    pub async fn read() -> Result<NetworkConfig, anyhow::Error> {
        let adapter = Local{};
        let backend = Wicked::new(adapter);
        backend.read().await
    }

    pub fn add_conn(&mut self, conn: Connection) {
        self.conns.push(conn)
    }

    pub fn add_device(&mut self, device: Device) {
        self.devices.push(device)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::connections::Connection;
    use crate::devices::{Device,DeviceType};

    #[test]
    fn test_add_connection() {
        let eth0 = Connection::new_ethernet("eth0".to_string());
        let mut config = NetworkConfig::new();
        config.add_conn(eth0);
        assert_eq!(config.conns.len(), 1);
    }

    #[test]
    fn test_add_device() {
        let dev = Device::new_physical("eth0".to_string(), DeviceType::Ethernet);
        let mut config = NetworkConfig::new();
        config.add_device(dev);
        assert_eq!(config.devices.len(), 1);
    }
}
