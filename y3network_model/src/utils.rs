use std::net::IpAddr;
use std::str::FromStr;
use std::fmt;
use serde::{Serialize,Deserialize};

#[derive(Debug,PartialEq,Serialize,Deserialize)]
pub struct Address {
    pub local: IpAddr,
    pub prefix: u8,
    pub label: Option<String>
}

impl FromStr for Address {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let parts: Vec<&str> = s.split('/').collect();

        if let Ok(ip) = IpAddr::from_str(parts[0]) {
            Ok(Address { local: ip, prefix: parts[1].parse().unwrap(), label: None })
        } else {
            Err(format!("Unknown device type: {}", &s))
        }
    }
}

impl fmt::Display for Address {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if let Some(label) = &self.label {
            write!(f, "{}/{} ({})", &self.local, &self.prefix, label)
        } else {
            write!(f, "{}/{}", &self.local, &self.prefix)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse() {
        let addr: Address = "192.168.122.1/24".parse().unwrap();
        assert_eq!(addr.local, IpAddr::from_str("192.168.122.1").unwrap());
        assert_eq!(addr.prefix, 24);
    }
}
