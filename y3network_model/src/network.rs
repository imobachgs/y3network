use crate::NetworkConfig;
use crate::backends::Backend;
use crate::adapters::Adapter;
use std::collections::HashMap;

/// This class represents the entry point to get network information
///
/// It is possible to handle several network configuration objects (the
/// current configuration, the wanted one, and so on), so each object
/// has an associated name.
#[derive(Default)]
pub struct Network {
    configs: HashMap<String,NetworkConfig>
}

impl Network {
    pub fn new() -> Self {
        Default::default()
    }

    /// Read the network configuration from the current system
    pub async fn read<S, T>(&mut self, backend: S) -> Result<(), anyhow::Error>
        where S: Backend<T>, T: Adapter
    {
        let config = backend.read().await?;
        self.configs.insert("system".to_owned(), config);
        Ok(())
    }

    /// Get the network configuration with the given name
    pub fn get(&self, name: &str) -> Option<&NetworkConfig> {
        self.configs.get(name)
    }

    /// Add a network configuration and assign it a name
    pub fn add(&mut self, name: String, config: NetworkConfig) {
        self.configs.insert(name, config);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_add_and_get() {
        let mut network = Network::new();
        assert!(network.get(&String::from("system")).is_none());

        network.add(String::from("system"), NetworkConfig::new());
        assert!(network.get(&String::from("system")).is_some());
    }
}
