use std::str::FromStr;
use crate::utils::Address;
use serde::{Serialize,Deserialize};

#[derive(Debug,Serialize,Deserialize,PartialEq,Clone)]
pub enum DeviceType {
    Loopback,
    Ethernet,
    Wifi,
    Bridge,
    Unknown(String),
}

impl Default for DeviceType {
    fn default() -> Self {
        DeviceType::Ethernet
    }
}
impl FromStr for DeviceType {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "loopback" => Ok(DeviceType::Loopback),
            "ethernet" => Ok(DeviceType::Ethernet),
            "wireless" => Ok(DeviceType::Wifi),
            "bridge" => Ok(DeviceType::Bridge),
            _ => Ok(DeviceType::Unknown(String::from(s)))
        }
    }
}

#[derive(Debug,PartialEq,Default,Serialize,Deserialize)]
pub struct Device {
    pub name: String,
    pub dev_type: DeviceType,
    pub is_virtual: bool,
    pub addresses: Vec<Address>
}

impl Device {
    fn new(name: String, dev_type: DeviceType, is_virtual: bool) -> Self {
        Device { name, dev_type, is_virtual, ..Default::default() }
    }

    pub fn new_virtual(name: String, dev_type: DeviceType) -> Self {
        Device::new(name, dev_type, true)
    }

    pub fn new_physical(name: String, dev_type: DeviceType) -> Self {
        Device::new(name, dev_type, false)
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_new_virtual_device() {
        let dev = Device::new_virtual("eth0".to_string(), DeviceType::Ethernet);
        assert_eq!(dev.name, "eth0".to_string());
        assert_eq!(dev.dev_type, DeviceType::Ethernet);
        assert_eq!(dev.is_virtual, true);
    }

    #[test]
    fn test_new_physical_device() {
        let dev = Device::new_physical("eth0".to_string(), DeviceType::Ethernet);
        assert_eq!(dev.name, "eth0".to_string());
        assert_eq!(dev.dev_type, DeviceType::Ethernet);
        assert_eq!(dev.is_virtual, false);
    }

    #[test]
    fn test_device_type_from_str() {
        let wifi = DeviceType::from_str("wireless").unwrap();
        assert_eq!(wifi, DeviceType::Wifi);

        let unknown = DeviceType::from_str("unknown").unwrap();
        assert_eq!(unknown, DeviceType::Unknown("unknown".to_string()));
    }
}
