use y3network_model::{Network,backends::Wicked,adapters::Local};

#[async_std::main]
async fn main() -> Result<(), anyhow::Error> {
    let mut network = Network::new();
    let backend = Wicked::new(Local{});
    network.read(backend).await.expect("Failed to read wicked configuration");
    let config = network.get(&"system".to_owned()).unwrap();

    println!("* Devices:");
    for dev in &config.devices {
        println!("  {} ({:?})", dev.name, dev.dev_type);
        for addr in &dev.addresses {
            println!("    {}", addr)
        }
    }

    println!("* Connections:");
    for conn in &config.conns {
        println!("  {} ({:?})", conn.name, conn.config);
        for addr in &conn.addresses {
            println!("    {}", addr)
        }
    }

    Ok(())
}
